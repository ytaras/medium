import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Container } from 'reactstrap'

import Landing from './Landing'
import About from './About'
import Tour from './Tour'
import Comments from './Comments'
import Contacts from './Contacts'
import NoMatch from './NoMatch'

import Navigation from './Navigation'
import Footer from './Footer'

const App = () => {
  return (
    <div className='app'>
      <Container>
        <Navigation />
        <Switch>
          <Route exact path='/' component={Landing} />
          <Route path='/about' component={About} />
          <Route path='/tour' component={Tour} />
          <Route path='/comments' component={Comments} />
          <Route path='/contacts' component={Contacts} />
          <Route component={NoMatch} />
        </Switch>
      </Container>
      <Footer />
    </div>
  )
}

export default App
