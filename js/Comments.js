import React from 'react'
import ReactDisqusThread from 'react-disqus-comments'

const Comments = () => {
  const style = {
    'margin-top': '2rem',
    'border-top-color': '#ffe0b2'
  }
  return (
    <div>
      <p className='comments'>
        Щодня ми приймаємо багато пацієнтів, інколи вони залишають свої відгуки про нашу роботу у нашому журналі в клініці.
      </p>
      <p className='comments'>
        Якщо Ви лікувалися у нас, у медичній клініці «Медіум», то, якщо Ваша ласка, залиште свій відгук тут.
      </p>
      <p className='comments'>
        Дякуємо заздалегідь!
      </p>
      <ReactDisqusThread
        shortname='medium-1'
        identifier='medium-disqus-thread'
        title='React Disqus thread component'
        url='https://medium.if.ua'
      />
      <hr style={style} />
    </div>
  )
}

export default Comments
