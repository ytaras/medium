import React, { PropTypes } from 'react'
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps'
import withScriptjs from 'react-google-maps/lib/async/withScriptjs'

const Map = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap defaultZoom={15} defaultCenter={props.markerPosition}>
      <Marker
        position={{
          lat: props.markerPosition.lat,
          lng: props.markerPosition.lng
        }}
        defaultAnimation={3}
      />
    </GoogleMap>
  ))
)
export default Map

Map.propTypes = {
  markerPosition: PropTypes.object,
  center: PropTypes.object
}
