import React from 'react'
import Map from './Map'
import Icon from './Icon'
import loadingElement from './loadingElement.js'

const Contacts = () => {
  const markerPosition = { lat: 48.919942, lng: 24.734502 }
  return (
    <div className='row p-2'>
      <div className='col-sm-4'>
        <h5 className='font-weight-bold text-center my-1'>
          Як нас знайти ?
        </h5>
        <hr />
        <Icon icon='addressCard' width={2} color='antiquewhite' />
        <p>
          вул. Кисілевської, 37<br /> м. Івано - Франківськ
        </p>
        <hr />
        <Icon icon='boss' width={2} color='antiquewhite' />
        <p>
          Шевчик Констянтин Борисович <br />
          Шевчик Наталія Петрівна
        </p>
        <hr />
        <Icon icon='clock' width={2} color='antiquewhite' />
        <p>
          9:00 - 19:00 (Пн. - Пт.)<br />
          По домовленості (Сб.)
        </p>
        <hr />
      </div>
      <div className='col-sm-8'>
        <Map
          containerElement={<div style={{ height: '460px' }} />}
          mapElement={<div style={{ height: `450px` }} />}
          loadingElement={loadingElement()}
          googleMapURL='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyByH0c5bxYDZ48BLQ401BBsm4DppG6QNkQ'
          markerPosition={markerPosition}
        />
      </div>
    </div>
  )
}

export default Contacts
