import React from 'react'
import { Jumbotron } from 'reactstrap'

const Landing = () => {
  return (
    <Jumbotron>
      <div className='text-sm-center'>
        <img
          src='_public/img/front_view.jpg'
          className='img-fluid img-thumbnail'
          alt='Responsive image'
        />
      </div>
      <h4 className='text-center comments'>
        Приватна стоматологічна клініка "Медіум" пропонує:
      </h4>
      <h5 className='text-center comments'>
        найсучасніше високотехнологічне діагностичне і лікувальне устаткування,
      </h5>
      <h5 className='text-center comments'>
        стоматологічні послуги з використанням передових методів лікування профілактика за допомогою сучасних технологій.
      </h5>
      <p className='comments'>Наші послуги:</p>
      <ul type='disc'>
        <li>Терапевтична стоматологія</li>
        <li>Лікування карієсу</li>
        <li>Терапія каналів зуба</li>
        <li>Професійна гігієна порожнини рота</li>
        <li>Вибілювання зубів</li>
        <li>Хірургічна стоматологія</li>
        <li>Дитяча стоматологія</li>
        <li>Естетична стоматологія</li>
        <li>Художня реставрація зубів</li>
        <li>Імплантація зубів</li>
        <li>Рентгенологічне дослідження зубів</li>
      </ul>
    </Jumbotron>
  )
}

export default Landing
