import React from 'react'
import { Container, Row } from 'reactstrap'
import Icon from './Icon'

const Footer = () => {
  return (
    <Container className='mt-1'>
      <Row className='justify-content-around align-items-center'>
        <div className='col-sm'>
          <div className='myFlexFooterItems' id='mobile'>
            <Icon icon='mobile' width={1.5} color='darkgrey' />
            <p>
              (0342) 73 17 78
              <br /> 067 704 43 14
              <br /> 099 226 47 31
            </p>
          </div>
        </div>
        <div className='col-sm'>
          <div className='myFlexFooterItems'>
            <Icon icon='envelope' width={2} color='darkgrey' />
            <p>med.if.ua@gmail.com</p>
          </div>
        </div>
        <div className='col-sm'>
          <div className='myFlexFooterItems'>
            <Icon icon='addressCard' width={2} color='darkgrey' />
            <p>
              вул. Кисілевської, 37
              <br />м. Івано - Франківськ
            </p>
          </div>
        </div>
      </Row>
    </Container>
  )
}
export default Footer
