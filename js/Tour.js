import React from 'react'
import { STREETVIEWS } from './constants.js'

const Tour = () => {
  return (
    <div className='iframes'>
      <h3>Віртуальний тур</h3>
      <p>Запрошуємо Вас здійснити віртуальний тур по всіх кабінетах стоматологічної клініки</p>

      <div>
        <iframe
          src={STREETVIEWS.hall}
          width='800'
          height='600'
          frameBorder='0'
          style={{ border: 0 }}
          allowFullScreen
        />
      </div>
      <div>
        <iframe
          src={STREETVIEWS.dentist1}
          width='800'
          height='600'
          frameBorder='0'
          style={{ border: 0 }}
          allowFullScreen
        />
      </div>
      <div>
        <iframe
          src={STREETVIEWS.dentist2}
          width='800'
          height='600'
          frameBorder='0'
          style={{ border: 0 }}
          allowFullScreen
        />
      </div>
    </div>
  )
}
export default Tour
