import React from 'react'

const About = () => {
  const style = {
    'margin-top': '2rem',
    'border-top-color': '#ffe0b2'
  }
  return (
    <div>
      <div className='text-sm-center mb-1'>
        <img
          src='_public/img/big_logo.png'
          className='img-fluid'
          alt='Responsive image'
        />
      </div>
      <p className='comments'>
        Уже 7 років клініка «Медіум» надає в Івано-Франківську комплексні послуги з терапевтичної стоматології, ортодонтичного лікування, ортопедії, імплантології, а віднедавна і косметології. У нас працюють фахівці найвищої кваліфікації.
      </p>
      <h3 className='text-center'>НАШ КОЛЕКТИВ:</h3>
      <p>Шевчик Констянтин Борисович</p>
      <ul type='disc'>
        <li>загальна стоматологія</li>
        <li>ортопедична стоматологія</li>
        <li>хірургічна стоматологія</li>
      </ul>
      <p>Шевчик Наталія Петрівна</p>
      <ul type='disc'>
        <li>терапевтична стоматологія</li>
      </ul>
      <hr style={style} />
    </div>
  )
}
export default About
